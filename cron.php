<?php
if (!$_SERVER['DOCUMENT_ROOT']) $_SERVER['DOCUMENT_ROOT'] = __DIR__;
require_once($_SERVER['DOCUMENT_ROOT'].'/local/prolog.php');
exec('sudo chown -R www-data:www-data '.__DIR__.'/logs/');

$App->init();
$arResult = $App->getData();
//dump($arResult);
$netrics = [];
$cases = [
    'CREATE_CASE' => 'CreateCase',
    'ADD_STEP_TO_CASE' => 'AddStepToCase',
    'ADD_CASE' => 'AddCase',
    'CLOSE_CASE' => 'CloseCase',
    'UPDATE_CASE' => 'UpdateCase',
];

foreach ($arResult as $serverName => $tables) {
    if (isset($tables['MEDCARDS']) && $tables['MEDCARDS']) {
        if (!$netrics['Pix']) $netrics['Pix'] = new Netrika();
        App::log("MS base ($serverName)","Записей на добавлeние ".count($tables['MEDCARDS'])." (MEDCARDS)");
        $res = $netrics['Pix']->addCard($tables['MEDCARDS']);
        
        if ($res) {
            $App->cons[$serverName]['DB']->deleteLog($res,'MEDCARDS');
            App::log("MS base ($serverName)","Очищено в логе добавления ".count($res)." (MEDCARDS)");
        }
        App::log("", str_repeat('-', 150));
    }
    
    foreach ($cases as $c => $m) {
        if (isset($tables[$c]) && $tables[$c]) {
            if (!$netrics['Emk']) $netrics['Emk'] = new Netrika('Emk');
            App::log("MS base ($serverName)","Записей на добавлeние ".count($tables[$c])." ($c)");
            $res = $netrics['Emk']->caseFunctions($tables[$c],$m);

            if ($res) {
                $App->cons[$serverName]['DB']->deleteLog($res,$c);
                App::log("MS base ($serverName)","Очищено в логе добавления ".count($res)." ($c)");
            }
            App::log("", str_repeat('-', 150));
        }
    }
}

if ($c = App::cleanLogs()) {
    App::log("Система","Удалено ".$c." детальных файлов логов (срок больше 7 дней)");
    App::log("", str_repeat('-', 150));
}
?>