<?php require_once($_SERVER['DOCUMENT_ROOT'].'/local/prolog.php');?>
<script src="/local/js/jquery.min.js"></script>

<?php $path = $_SERVER['DOCUMENT_ROOT'].'/logs/';
$dir = scandir($path);
usort($dir,function($a,$b){
    $a = str_replace(['log_','.txt'], '', $a);
    $b = str_replace(['log_','.txt'], '', $b);
    return strtotime($a) < strtotime($b);
});

foreach ($dir as $k=>$file) {
    if (is_file($path.$file) && strpos($file,'.php') === false) {?>
        <a class="showlogs" href="/logs/<?=$file?>"><?=$file?></a><br/>
    <?php }
}
?>
<p id="log"></p>

<script>
$(function(){
    var log = $('#log');
    $('.showlogs').on('click',function(){
        var _self = $(this);
        $.ajax({
            url: _self.attr('href'),
            type: 'POST',
            success: function(res){
                res = res.split('\n').join('<br/>');
                log.html(res);
            }
        });
        return false;
    });
})
</script>