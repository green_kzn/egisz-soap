<?php
spl_autoload_register('Autoloader::loadByName');

class Autoloader
{
    /**
     * Базовая директория для загрузки
     *
     * @var string
     */
    protected static $_autoloadRootPath = [
        __DIR__."/"
    ];

    /**
     * Рекурсивный поиск по подразделам
     *
     * @param string $className - имя класса для поиск
     * @param string $directoryPath - абсолютный путь к разделу для поиска
     * @return mixed string|boolean Возвращает путь к файлу или false
     **/
    protected static function searchClassRecursive($className, $directoryPath)
    {
        $classPath = $directoryPath . $className . '.php';
        if (file_exists($classPath)) {
            return $classPath;
        } elseif ($directoryItems = scandir($directoryPath)) {
            foreach ($directoryItems as $item) {
                if ($item == '.' || $item == '..') continue;
                if (is_dir($directoryPath . $item)) {
                    if ($recursiveResult = self::searchClassRecursive($className, $directoryPath . $item . DIRECTORY_SEPARATOR)) {
                        return $recursiveResult;
                    }
                }
            }
        }
        return false;
    }
    /**
     * Загрузка класса по имени
     *
     * @param string $className - имя класса для загрузки
     * @return boolean
     */
    public static function loadByName($className)
    {
        $class_path = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        $class_path = str_replace('trait', '', $className);
        
        foreach (self::$_autoloadRootPath as $directory) {
            if ($classPath = self::searchClassRecursive($class_path, $directory)) {
                require_once($classPath);
                return true;
            }
        }
        return false;
    }
}
?>