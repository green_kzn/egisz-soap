<?php
//error_reporting(E_ALL);ini_set('display_errors','on');
ini_set('display_errors','off');
define('DEBUG_MODE',0);
date_default_timezone_set('Etc/GMT-4');

require_once __DIR__.'/Autoloader.php';

global $DB,$App;
$DB = new DB(Config::mysql);
$App = new App();

function dump($d){
	echo '<pre>';
//	print_r('<b style="font-size: 30px">'.++$GLOBALS['ddd'].'</b>');
	print_r($d);
	echo '</pre>';
}

function dd($d){
    echo '<pre>';
    if (is_string($d)) print_r(htmlentities($d));
    else print_r($d);

    echo '</pre>';
    die();
}

function getNamedParam($paramStr, $name, $default){
    if (preg_match("~<$name>(.*)</$name>~", $paramStr, $match)){
        return $match[1];
    }
    return $default;
}

//        dump('<b style="font-size: 30px">'.++$GLOBALS['ddd'].'</b>');
//        dump($ar)
//

set_exception_handler ( function ($e){
    dd($e);
} );