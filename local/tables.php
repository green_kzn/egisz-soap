<?php require_once($_SERVER['DOCUMENT_ROOT'].'/local/prolog.php');
$res = $DB->query('
CREATE TABLE IF NOT EXISTS `uploadData` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `serverName` varchar(100) NOT NULL,
    `table` varchar(11) NOT NULL,
    `tid` int(11) NOT NULL,
    `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;',true);

dump($res);

$res = $DB->query('
CREATE TABLE IF NOT EXISTS `medcards` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `BirthDate` TIMESTAMP NOT NULL,
    `DeathTime` DATETIME DEFAULT NULL,
    `FamilyName` varchar(100) NOT NULL,
    `GivenName` varchar(100) NOT NULL,
    `MiddleName` varchar(100) DEFAULT NULL,
    `IdBloodType` TINYINT UNSIGNED DEFAULT NULL,
    `IdPatientMIS` int(11) NOT NULL,
    `Sex` TINYINT UNSIGNED NOT NULL,
    `SocialGroup` TINYINT UNSIGNED DEFAULT NULL,
    `SocialStatus` varchar(100) DEFAULT NULL,
    `Documents` varchar(255) DEFAULT NULL,
    `Addresses` varchar(255) DEFAULT NULL,
    `BirthPlace` varchar(255) DEFAULT NULL,
    `ContactDto` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

dump($res);

$res = $DB->query('
CREATE TABLE IF NOT EXISTS `log` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `action` varchar(50) NOT NULL,
    `message` varchar(255) NOT NULL,
    `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

dump($res);