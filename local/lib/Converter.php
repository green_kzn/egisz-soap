<?php
class Converter {
    /**
     * Соответсвие полей с карточкой пациента
     * @var array 
     */
    public static $matchMedcard = [
        'ID' => 'IdPatientMIS',
        'GENDERTYPE' => 'Sex', // узнать про значения
        'BIRTHDATE' => 'BirthDate',
        'NAME' => 'FamilyName',
        'NAME1' => 'GivenName',
        'NAME2' => 'MiddleName',
        'DOC_TYPE' => 'IdDocumentType', // связка из таблицы DOCUMENTS
        'DOC_SERIES' => 'DocS',
        'DOC_NUMBER' => 'DocN',
        'DOC_DATESTOP' => 'ExpiredDate',
        'DOC_DATE' => 'IssuedDate',
        'DOC_CONTENT' => 'ProviderName',
        'FULLADDRESS' => 'IdAddressType', // нужно выбирать смотря что заполненно
        'FULLADDRESS' => 'StringAddress',
        'FULLADDRESS_ACTUAL' => 'StringAddress',
        'PHONE' => 'ContactValue',
        'PHONE1' => 'ContactValue',
        'EMAIL' => 'ContactValue',
        'FAX' => 'ContactValue',
    ];
    
    /**
     * Обязательные поля для карточки пациента
     * @var array 
     */
    public static $structureAddPatient = [
        'Addresses' => [
            'AddressDto' => [
                'IdAddressType' => 1,
                'StringAddress' => 1,
            ],
        ],
        'BirthDate' => 1,
        'Contacts' => [
            'ContactDto' => [
                'ContactValue' => 1,
                'IdContactType' => 1,
            ]
        ],
        'Documents' => [
            'DocumentDto' => self::IdentityDocument
        ],
        'FamilyName' => 1,
        'GivenName' => 1,
        'MiddleName' => 0,
        'IdPatientMIS' => 1,
        'Sex' => 1,        
    ];
    
    const humanName = [
        'FamilyName'=> 1,
        'GivenName'=> 1,
        'MiddleName'=> 0,
    ];
    
    const IdentityDocument = [
        'DocN' => 1,
        'DocS' => 1,
        'IdDocumentType' => 1,
        'IssuedDate' => 0,
        'ProviderName' => 1,
        'ExpiredDate' => 0,
    ];
    
    const DoctorInCharge = [
        'IdSpeciality' => 1,
        'IdPosition' => 1,
        'Person' => [
            'HumanName' => self::humanName,
            'IdPersonMis'=>1,
            'Documents' => [
                'IdentityDocument' => self::IdentityDocument
            ],
        ],
    ];
    
    public static $structureCreateCase = [
        'OpenDate' => 1,
        'CloseDate' => 0,
        'HistoryNumber' => 1,
        'IdCaseMis' => 1,
        'IdPaymentType' => 1,
        'Confidentiality' => 1,
        'DoctorConfidentiality' => 1,
        'CuratorConfidentiality' => 1,
        'IdCaseResult' => 0,
        'Comment' => 0,
        'IdPatientMis' => 1,
        'IdCasePurpose' => 0,
        'IdCaseType' => 1,
        'IdAmbResult' => 0,
        'DoctorInCharge' => self::DoctorInCharge,
        'Authenticator' => [
            'Doctor' => self::DoctorInCharge,
        ],
        'Author' => [
            'Doctor' => self::DoctorInCharge,
        ],
        'Steps' => [
            'StepAmb' => [
                'Doctor' => self::DoctorInCharge,
                'DateStart' => 1,
                'DateEnd' => 1,
                'IdStepMis' => 1,
                'IdPaymentType' => 0,
                'IdVisitPlace' => 1,
                'IdVisitPurpose' => 1,
            ]
        ]
    ];
    
    /**
     * поля для конвертации case запросов
     * @var type 
     */
    public static $matchCase = [
        'ID' => 'HistoryNumber',
        'DATETIME' => 'OpenDate',
        'RECEPTIONCLOSEDATETIME' => 'CloseDate',
        'INFO' => 'Comment',
        'RECEPTIONTYPE' => 'IdCasePurpose',
        'RECEPTIONTYPEEVENTSERVICE' => 'IdCaseType',
    ];
    
    /**
     * Дни в дату
     * @param type $v
     * @return type
     */
    static function daysToDate($v = 2,$null_time = false){
        if ($null_time) $format = 'Y-m-d\T00:00:00';
        else $format = 'Y-m-d\TH:i:s';
        return date($format,($v-2)*24*60*60 - abs(strtotime('01.01.1900')));
        //return date('Y-m-d',($v-2)*24*60*60 - abs(strtotime('01.01.1900')));
    }
    
    /**
     * Дату в дни
     * @param type $v
     * @return type
     */
    static function dateToDays($v = '01.01.1900'){
        $dates_diff = date_diff(date_create($v), date_create('01.01.1900'));
        return $dates_diff->days+2;
    }
}