<?php class App {
       
    public $cons = [];
    
    public $hostFromDBS = 'https://admin.archimed-soft.ru';
    public $tokenDBS = 'Y2tYRVNtQzRTcW4zOThKVmhvTWJBSWxXZ3V0T1RySjZzYldmMUIyazdSSysxZHdtaTJTRWVZSGpzK1VqVnkzVkhERUtmb3ZRSVB4azZoSitFZ1hueUE9PQ==';
    
    public function __construct() {
        //$this->init();
    }
    
    /**
     * Основной метод инициализирующий соединения с Базами Mssql
     */
    public function init(){
        $data = file_get_contents($this->hostFromDBS.'/api/get-all?project=egisz&token='.$this->tokenDBS);
        $data = json_decode(ReversCrypt::mc_decrypt($data),true);
        foreach ($data as $k=>$v) {
            if (filter_var($v['connection']['host'], FILTER_VALIDATE_IP)) {
                Config::$DBS[$v['settings']['idlpu']] = [
                    'host' => $v['connection']['host'],
                    'port' => $v['connection']['port'],
                    'db' => $v['connection']['db'],
                    'user' => $v['connection']['login'],
                    'pass' => $v['connection']['pass'],
                    'guid' => $v['settings']['guid'],
                    'idLPU' => $v['settings']['idlpu']
                ];
            }
        }

        foreach (Config::$DBS as $k => $settings) {
            $rDB = new MsDB($settings);
            $this->cons[$k]['idLPU'] = $settings['idLPU'];
            $this->cons[$k]['guid'] = $settings['guid'];
            $this->cons[$k]['DB'] = $rDB;
        }
    }
    
    /**
     * Выборка днных из всех серверов
     * @return type
     */
    public function getData(){
        $arResult = [];
        foreach ($this->cons as $k => $rDB) {
            $arResult[$k] = $this->getDataFromMsDB($k);
        }
        return $arResult;
    }
    
    /**
     * Выборка данных которые нужно обновить
     * @param string $nameServer - текущий сервер mssql
     * @return type
     */
    protected function getDataFromMsDB( $nameServer = '' ){
        $arTablesJoin = [];
        if ($nameServer) {
            $con = $this->cons[$nameServer];
            $rDB = $con['DB'];
			$cases = [
				'CREATE_CASE' => 1,
				'ADD_STEP_TO_CASE' => 1,
				'ADD_CASE' => 1,
				'CLOSE_CASE' => 1,
				'UPDATE_CASE' => 1,
			];
            $arResult = $rDB->query("SELECT * FROM $rDB->logTable");
            if ($arResult) {                
                foreach ($arResult as $arItem) {
                    $arTablesJoin[$arItem['NAME']][] = $arItem['TID'];
                }
                foreach ($arTablesJoin as $table => $ids) {
                    $ids = array_unique($ids);
                    if ($table == "MEDCARDS") {
                        $medcards = new Medcards($ids,$con);
                        $arTablesJoin[$table] = $medcards->arResult;
                    } elseif (isset($cases[$table])) {
                        //CREATE_CASE
                        //ADD_STEP_TO_CASE
                        //ADD_CASE
                        //CLOSE_CASE
                        //UPDATE_CASE
                        $reception = new Reception($ids,$con,$table);
                        $arTablesJoin[$table] = $reception->arResult;
                    }
                }
            }
        }
        
        return $arTablesJoin;
    }
    
        
    /**
     * Логирование действий
     * @param string $action
     * @param string $mess
     */
    public static function log($action = '',$mess=''){
        $mess = ($action ? date('d.m.Y H:i:s').' '.$action.': ' : '').$mess.chr(10);
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/logs/log_'.date('d.m.Y').'.txt', $mess,FILE_APPEND);
        echo $mess."<br>";
    }
    
    /**
     * Создание лог-файл php массива
     * @param mixed $mess
     * @return link
     */
    public static function logPhp($mess){
        $mess = var_export($mess,true);
        $name = 'detail_'.date('d.m.Y H:i:s').'.txt';
        $path = '/logs_detail/'.$name;
        file_put_contents($_SERVER['DOCUMENT_ROOT'].$path, $mess);
        dump($mess);
        return '<a href="'.$path.'" target="_blank">'.$name.'</a>';
    }
    
    /**
     * Создание лог-файл для xml
     * @param xml $mess - xml данные
     * @param string $suffix - суффик для типа файла
     * @return link
     */
    public static function logXml($mess,$suffix = ''){
        $name = 'detail_'.date('d.m.Y H:i:s').($suffix?'_'.$suffix:'').'.xml';
        $path = '/logs_detail/'.$name;
        file_put_contents($_SERVER['DOCUMENT_ROOT'].$path, $mess);
        dump($mess);
        return '<a href="'.$path.'" target="_blank">'.$name.'</a>';
    }
    
    /**
     * Метод чистит устаревшие детальные логи раз в 7 дней
     * и возвращает кол-во удаленных файлов
     * @param string $folder
     * @param int $limit
     * @return int
     */
    public static function cleanLogs($folder = '/logs_detail/',$limit = 604800){ // 7 дней
        $path = $_SERVER['DOCUMENT_ROOT'].$folder;
        $dir = scandir($path);
        $c = 0;
        foreach ($dir as $k=>$file) {
            if (is_file($path.$file)) {
                $date = preg_match('~^[a-z]+_([\d:\.\s]+)~iu', $file,$match);
                if ($match[1]) {
                    $t = time() - strtotime($match[1]);
                    if ($t > $limit) {
                        unlink($path.$file); // прошла неделя-файл удален
                        $c++;
                    }
                }
            }
        }
        return $c;
    }
}

