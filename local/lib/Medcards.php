<?php
class Medcards {
    
    private $rDB;
    private $con;

    public $arResult = [];
    
    /**
     * Основной метод, формирует валидный список карточек пациентов перед отправкой в Нетрику
     * здесь формируется сразу 3 xml блока на все случаи жизни)
     * @param array $ids - id карточек пациентов
     * @param array $con - массив настроек подключения к бд Mssql
     * @return array
     */
    public function __construct($ids = [],$con = []) {
        $this->con = $con;
        $this->rDB = $con['DB'];
        $arItems = $this->rDB->query("SELECT * FROM MEDCARDS WHERE ID IN (".implode(',',$ids).")");
        if ($arItems) {
            foreach ($arItems as $k=>$item) {
                if ($tmp = $this->changeFields($item)) {
                    $xml = [
                        'xml' => [
                            'GetPatient' => $this->getPatient($tmp),
                            'AddPatient' => $this->addPatient($tmp),
                            'UpdatePatient' => $this->updatePatient($tmp),
                        ],
                        'info' => $tmp,
                    ];
                    $this->arResult[$item['ID']] = $xml;
                }
            }
        } else {
            $this->rDB->deleteLog($ids);
        }
    }
    
    /**
     * Перевод данных из бд mssql в mysql
     * @param array $ar - массив из mssql
     * @return array
     */
    public function changeFields( $ar = []){
        $tmp = [];
        $dateFileds = [
            'CREATIONDATE' => 1,
            'REGDATE' => 1,
            'BIRTHDATE' => 1,
            'DOC_DATE' => 1,
            'DOC_DATESTOP' => 1,
            'LASTRECEPTIONDATE' => 1,
            'LASTTALONDATE' => 1,
        ];
        $documents = [
            'DOC_TYPE'=>1,
            'DOC_SERIES'=>1,
            'DOC_NUMBER'=>1,
            'DOC_DATESTOP'=>1,
            'DOC_DATE'=>1,
            'DOC_CONTENT'=>1,
        ];
        $contacts = [
            'PHONE'=>1,
            'PHONE1'=>2,
            'EMAIL'=>3,
            'FAX'=>4,
        ];
        $adres = [
            'FULLADDRESS' => 1,
            'FULLADDRESS_ACTUAL' => 2,
        ];

        foreach ($ar as $k=>$v) {
            $t = isset(Converter::$matchMedcard[$k]) ? Converter::$matchMedcard[$k]: false;
            if ($t && $v != null) {
                $needSet = true;
                if( $k == 'GENDERTYPE') {
                    $v = intval($v)+1;
                }
                elseif (isset($documents[$k])) {
                    if ($v) {
                        if (isset($dateFileds[$k])) {
                            $v = Converter::daysToDate($v);
                        }
                        if( $k == 'DOC_TYPE') {
                            $v = $this->rDB->query("SELECT CODE_EGISZ FROM DOCUMENTS WHERE ID=$v");
                            $v = current($v)['CODE_EGISZ'];
                        }
                        $tmp['Documents']['DocumentDto'][$t] = $v;
                    }
                    $needSet = false;
                } elseif (isset($dateFileds[$k])) {
                    $v = Converter::daysToDate($v,$k == 'BIRTHDATE');
                }
                elseif(isset($adres[$k]) && $v) {
                    $z = [
                        'IdAddressType' => $adres[$k],
                        'StringAddress' => $v,
                    ];
                    $tmp['Addresses']['AddressDto'][] = $z;
                    $needSet = false;
                }
                elseif(isset($contacts[$k]) && $v) {
                    $z = [
                        'ContactValue' => $v,
                        'IdContactType' => $contacts[$k],
                    ];
                    if ($contacts[$k] != 3) {
                        preg_match('~[a-zа-я_]~iu', $v,$match);
                    }
                    
                    if (!$match) $tmp['Contacts']['ContactDto'][] = $z;
                    $needSet = false;
                }
                
                if ($needSet) $tmp[$t] = (string)$v;
            }
        }
        
        if (!self::recursCheckRequired(Converter::$structureAddPatient,$tmp)) {
            $tmp = [];
            App::log("Проверка полей","Запись с id=".$ar['ID']." не будет выгружена");
        }
        
        if (!self::checkTypes($tmp)) {
            $tmp = [];
            App::log("Проверка полей на тип","Запись с id=".$ar['ID']." не будет выгружена");
            $this->rDB->deleteLog([$ar['ID']]);
        }
        
        
        // Важен порядок перечисления        
        $arResult = [];
        foreach (Converter::$structureAddPatient as $k=>$v) {
            if ($tmp[$k]) {
                if ($k == 'Documents') {
                    foreach ($v['DocumentDto'] as $j=>$val) {
                        if ($tmp[$k]['DocumentDto'][$j]) $arResult['Documents']['DocumentDto'][$j] = $tmp[$k]['DocumentDto'][$j];
                    }
                }
                else $arResult[$k] = $tmp[$k];
            }
        }
        
        $arResult['IdPatientMIS'] = md5($arResult['IdPatientMIS'].$arResult['BirthDate'].$arResult['Sex']);
        if ($midName = $arResult['MiddleName']) {
            $sex = $arResult['Sex'];
            unset($arResult['MiddleName']);
            unset($arResult['Sex']);
            $arResult['MiddleName'] = $midName;
            $arResult['Sex'] = $sex;
        }
        return $arResult;
    }
    
    /**
     * Метод проверяет поля на соответсвие типа данных
     * @param array $ar - массив данных
     * @return boolean
     */
    public static function checkTypes($ar = []){
        $res = false;
        if (mb_strlen($ar['FamilyName']) > 1 && mb_strlen($ar['GivenName']) > 1) $res = true;
        
        if (ctype_digit($ar['FamilyName']) || ctype_digit($ar['GivenName'])) $res = false;
        
        if ($ar['MiddleName'] && intval($ar['MiddleName']) > 0) $res = false;

        return $res;
    }


    /**
     * Метод проверяет обязательности полей и корректирует массив перед отправкой в нетрику
     * @param array $a - массив с обязательными полями
     * @param array $b - входящий массив
     * @param int $depth - глубина рекурсии
     * @return boolean
     */
    public static function recursCheckRequired($a,&$b,$depth = 0){
        $i = array_intersect_key($a,$b);
        $x = array_count_values($a);
        $y = array_count_values($i);
        
        if (!$depth) {
            $ar = [
                'Document'=>'s',
                'Contact'=>'s',
                'Address'=>'es',
            ];
            foreach ($ar as $k=>$v) {
                if ($v) {
                    if ($i[$k.$v]) {
                        $t = $b[$k.$v][$k.'Dto'];
                        if (isset($t[0])) {
                            foreach ($t as $j=>$item) {
                                $res = self::recursCheckRequired($i[$k.$v][$k.'Dto'],$item,1);
                                if (!$res) unset($b[$k.$v][$k.'Dto'][$j]);
                            }
                            if (empty($b[$k.$v])) unset($b[$k.$v]);
                        } else {
                            $res = self::recursCheckRequired($i[$k.$v][$k.'Dto'],$t,1);
                            if (!$res) unset($b[$k.$v]);
                        }
                    }
                } else {
                    if ($i[$k]) {
                        $res = self::recursCheckRequired($i[$k],$b[$k],1);
                        if (!$res) unset($b[$k]);
                    }
                }
            }
        }

        if ($x[1] == $y[1]) {
            return true;
        }
        
        return false;
    }
    
    /**
     * Метод строит xml в нужном формате согласно исходным данным
     * @param array $ar - массив данных
     * @param array $opt - параметры для рекурсивной передачи
     * @return xml
     */
    private function setRecursiveFields($ar,$opt = []){
        $options = array_merge([
            'key' => '',
            'tag' => 'a'
        ],$opt);

        $res = '';
        $lastTag = true;
        
        if ($options['key'] && is_string($options['key'])) $res .= "<".$options['tag'].":".$options['key'].'>';
        if (is_array($ar)) {
            $res .= "\n";
            foreach ($ar as $k=>$v) {
                $res .= $this->setRecursiveFields($v, array_merge($options,['key'=>$k]));
                if (!is_string($k)) {
                    $res .= "</".$options['tag'].":".$options['key'].">"."\n";
                    $lastTag = false;
                    if ($k+1 < count($ar)) $res .= "<".$options['tag'].":".$options['key'].'>'."\n";
                }
            }
        } else {
           $res .= $ar;
        }
        if ($options['key'] && is_string($options['key']) && $lastTag) $res .= "</".$options['tag'].":".$options['key'].">"."\n";
        return $res;
    }
    
    /**
     * Метод формирует request xml для Нетрики
     * Добавление пациента
     * @param array $d - массив данных
     * @return xml
     */
    public function addPatient($d = []){
        $request = '
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                <s:Body>
                    <AddPatient xmlns="http://tempuri.org/">
                        <guid>'.$this->con['guid'].'</guid>
                        <idLPU>'.$this->con['idLPU'].'</idLPU>
                        <patient xmlns:a="http://schemas.datacontract.org/2004/07/EMKService.Data.Dto" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">'."\n";
                        $request .= $this->setRecursiveFields($d);
                        $request .= '
                        </patient>
                    </AddPatient>
                </s:Body>
            </s:Envelope>';
        return $request;
    }
    
   /**
     * Метод формирует request xml для Нетрики
     * Обновление пациента
     * @param array $d - массив данных
     * @return xml
     */
    public function updatePatient($d = []){
        $request = '
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                <s:Body>
                    <UpdatePatient xmlns="http://tempuri.org/">
                        <guid>'.$this->con['guid'].'</guid>
                        <idLPU>'.$this->con['idLPU'].'</idLPU>
                        <patient xmlns:a="http://schemas.datacontract.org/2004/07/EMKService.Data.Dto" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">'."\n";
                        $request .= $this->setRecursiveFields($d);
                        $request .= '
                        </patient>
                    </UpdatePatient>
                </s:Body>
            </s:Envelope>';
        return $request;
    }
    
    /**
     * Метод формирует request xml для Нетрики
     * Поиск пациента для обновления
     * @param array $d - массив данных
     * @return xml
     */
    public function getPatient($d = []){
        $request = '
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://schemas.microsoft.com/2003/10/Serialization/">
                <s:Body>
                    <GetPatient xmlns="http://tempuri.org/">
                        <guid>'.$this->con['guid'].'</guid>
                        <idLPU>'.$this->con['idLPU'].'</idLPU>
                        <patient xmlns:emk="http://schemas.datacontract.org/2004/07/EMKService.Data.Dto">
                            <emk:IdPatientMIS>'.$d['IdPatientMIS'].'</emk:IdPatientMIS>
                        </patient>
                        <idSource>Reg</idSource>
                    </GetPatient>
                </s:Body>
            </s:Envelope>';
        return $request;
    }
}