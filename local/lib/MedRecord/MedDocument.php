<?php


abstract class MedDocument {

    protected $epmz;
    protected $author;
    protected $db;
    protected $exDataFieldName = 'EXDATAXML';

    function __construct($db, $epmz, $author) {
        $this->epmz = $epmz;
        $this->db = $db;
        $this->author = $author;
    }

    function get(){
        $data = [
            '@attributes' => [
                "xmlns" => "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc",
                'i:type' => $this->getType()
            ],
            "Attachments" => [
                "MedDocumentDto.DocumentAttachment" => $this->getAttachments()
            ],
            'Author' => $this->author,
            "CreationDate" => Converter::daysToDate($this->epmz['CREATIONDATETIME']),
            "Header" => $this->getHeader(),
            "IdDocumentMis" => $this->epmz['ID'],
        ];
        if (!$this->exDataFieldName){
            unset($data['Attachments']);
        }
        return $data;
    }

    function test(){
        $epmz = $this->db->query("select top 1 * from epmz where id = 5 ");

        $this->epmz = current($epmz);
//        dd($this->epmz);
        return $this->get();
    }

    protected abstract function getType();

    private function getHeader(){
        $types = $this->db->query("select et.name from epmz_types et left join epmz e on e.epmztype = et.id where e.id = ".$this->epmz['ID']);
        return current($types)['name'];
    }

    private function getAttachments(){
        $a = [
            "Data" => base64_encode($this->epmz[$this->exDataFieldName]),
            "MimeType" => 'text/xml',
        ];
        return [$a];
    }

}