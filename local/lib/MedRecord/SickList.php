<?php


class SickList extends MedDocument {

    protected $exDataFieldName = 'EXPARAMSTR';

    protected function getType() {
        return 'SickList';
    }

    function get() {
        $epmz = $this->epmz;
        $works = self::getExemptionFromWorks($epmz);

        $closed = getNamedParam($epmz['EXPARAMSTR'],'SICKLISTCLOSED','') == -1;
        $startDate = getNamedParam($works[0],'FROMDATE', '');

        $endDate = '';
        if ($closed){
            foreach (array_reverse($works) as $item) {
                if ($date = getNamedParam($item,'TODATE', 0)){
                    $endDate = Converter::daysToDate($date);
                }
            }
        }

        return array_merge( parent::get(), [
            "SickListInfo" => [
                "Number" => getNamedParam($epmz['EXPARAMSTR'],'SLNUMBER', ''),
                "DateStart" =>  $startDate ? Converter::daysToDate($startDate, true) : '',
                "DateEnd" => $endDate,
                "DisabilityDocReason" => self::getDisabilityDocReason($epmz),
                "DisabilityDocState" => $closed ? 3 : 1,
                "IsPatientTaker" => 'true',
            ]
        ]);
    }

    private static function getExemptionFromWorks($epmz){
        $result = [];
        for ($i = 0; $i <= 2; $i++){
            $result[] = htmlspecialchars_decode(getNamedParam($epmz['EXPARAMSTR'],"EXEMPTIONFROMWORK_$i", ''));
        }
        return $result;
    }

    private static function getDisabilityDocReason($epmz){
        $causeOfDisability = getNamedParam($epmz['EXPARAMSTR'],'CAUSEOFDISABILITY_CODE_TEXT', '');
        //(1 = 01; 2, 3 = 09; 4 = 03; 5 = 10; 6 = 05; 7 = 08)
        switch ($causeOfDisability){
            case '01' : return 1;
            case '09': return 3;
            case '03': return 4;
            case '10': return 5;
            case '05': return 6;
            case '08': return 7;
            default: return 1;
        }
    }

}