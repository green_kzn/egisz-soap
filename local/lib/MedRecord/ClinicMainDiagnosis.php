<?php


class ClinicMainDiagnosis extends Base {

    protected $epmz;
    protected $doctor;
    protected $db;
    protected $exDataFieldName = 'EXPARAMSTR';

    function __construct($db, $epmz, $doctor) {
        $this->epmz = $epmz;
        $this->db = $db;
        $this->doctor = $doctor;
    }

    function get(){
        $mkb10Name = getNamedParam($this->epmz[$this->exDataFieldName], 'MKB10_NAME', '');
        $mkb10Code = getNamedParam($this->epmz[$this->exDataFieldName], 'MKB10_CODE', '');
        $mkb10Code = preg_replace('~([A-Za-zА-Яа-я][0-9]+\.[0-9]+)([A-Za-zА-Яа-я])~iu', '$1', $mkb10Code);
        return [
            '@attributes' => [
                "xmlns" => "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag",
                'i:type' => 'ClinicMainDiagnosis',
            ],
            'DiagnosisInfo' => [
                'DiagnosedDate' => Converter::daysToDate($this->epmz['CREATIONDATETIME']),
                'IdDiagnosisType' => 1,
                'Comment' => $mkb10Name,
                'MkbCode' => $mkb10Code,
            ],
            'Doctor' => $this->doctor
        ];
    }


}