<?php
class Reception {
    private $rDB;
    private $con;
    private $method;
    
    public $arResult = [];
    
    /**
     * Основной метод, формирует валидный список карточек пациентов перед отправкой в Нетрику
     * @param array $ids - id записей в ресепшен
     * @param array $con - массив настроек подключения к бд Mssql
     * @param string $type - используемый метод
     * @return array
     */
    public function __construct($ids = [],$con = [],$type='CREATE_CASE') {
        $this->con = $con;
        $this->rDB = $con['DB'];
        $this->changeType($type);
        if ($type == 'ADD_STEP_TO_CASE') {
            $arItems = $this->rDB->query("SELECT * FROM EPMZ WHERE enabled=1 and ID IN (".implode(',',$ids).")");
        } else {
            $arItems = $this->rDB->query("SELECT * FROM RECEPTIONS WHERE enabled=1 and ID IN (".implode(',',$ids).")");
        }
        if ($arItems && $this->method) {
            $tmp = [];
            foreach ($arItems as $k=>$item) {
                //dump($item);
                if ($item = $this->{$this->method.'_changeFields'}($item)) {
                    $xml = [
                        'xml' => $this->{$this->method}($item),
                        'info' => $item
                    ];
                    if ($_REQUEST['xml']) dd($xml['xml']);
                    if ($type == 'ADD_STEP_TO_CASE') {
                        $this->arResult[$item['Step']['IdStepMis']] = $xml;
                    } else {
                        $this->arResult[$item['IdCaseMis']] = $xml;
                    }
                }
            }
            //dump($this->arResult);
        } else {
            //$this->rDB->deleteLog($ids);
        }
    }
    
    private function changeType($type='CREATE_CASE'){
        $type = strtolower($type);
        $t = explode('_',$type);
        foreach ($t as $k=>$v) {
            if ($k > 0) $t[$k] = ucfirst($v);
        }
        $this->method = implode('',$t);
    }
    
    /**
     * Перевод данных из бд mssql в mysql
     * !!! очень важен порядок элементов в массиве
     * @param array $ar - массив из mssql
     * @return array
     */
    public function addCase_changeFields( $ar = []){
        $arResult = [];
        $dateFileds = [
            'RECEPTIONDATETIME' => 1,
            'DATETIME' => 1,
            'RECEPTIONCLOSEDATETIME' => 1,
        ];
        
        $dopAr = [];
        
        foreach ($ar as $k=>$v) {
            $t = isset(Converter::$matchCase[$k]) ? Converter::$matchCase[$k]: false;
            if ($t && $v != null) {
                if (isset($dateFileds[$k])) $v = Converter::daysToDate($v,($t == 'OpenDate' /*|| $t == 'CloseDate'*/));
                $dopAr[$t] = (string)$v;
            }
            
        }
        
        $arResult['OpenDate'] = $dopAr['OpenDate'];
        $arResult['CloseDate'] = $dopAr['CloseDate'];
        $arResult['HistoryNumber'] = $dopAr['HistoryNumber'];
        $arResult['IdCaseMis'] = $dopAr['HistoryNumber'];
        $arResult['IdPaymentType'] = '1';
        $arResult['Confidentiality'] = '1';
        $arResult['DoctorConfidentiality'] = '1';
        $arResult['CuratorConfidentiality'] = '1';
        $arResult['IdLpu'] = $this->con['idLPU'];
        $arResult['IdCaseResult'] = '1';
        $arResult['Comment'] = $dopAr['Comment']?:'-';
        
        $Steps = [];
        $medRecs = [];

        $epmz = $this->rDB->query("SELECT * FROM EPMZ WHERE ENABLED=1 AND RECEPTIONID=".$ar['ID']);
        //dump($epmz);
        $DoctorInCharge = $this->getDoctors($ar);
        $DoctorInChargeAttr = $this->copyAddXmlns($DoctorInCharge);

        foreach ($epmz as $e) {
            $cTime = Converter::daysToDate($e['CREATIONDATETIME']);
            $mkb10 = $this->rDB->query("SELECT * FROM EPMZ_MKB10 WHERE ENABLED=1 AND EPMZID=".$e['ID']);
            //dump($mkb10);
            $medRecsSteps = [];
            $epmzDoc = $this->getDoctors($e,true);
            $epmzDocAttr = $this->copyAddXmlns($epmzDoc);
            foreach ($mkb10 as $m) {
                $accid = current($this->rDB->query("SELECT * FROM ACCOUNTS_CONTENTS WHERE ENABLED=1 AND ID=".$m['ACCID']));
                //dump($accid);
                if ($accid) {
                    if ($accid['OWNERACCOUNT'] > 0 && $accid['POLISEID'] > 0) {
                        $police = current($this->rDB->query("SELECT TYPEOMS FROM POLISES WHERE ENABLED=1 AND ID=".$accid['POLISEID']));
                        if ($police['TYPEOMS'] == 1) $arResult['IdPaymentType'] = 1;
                        else if ($police['TYPEOMS'] > 1) $arResult['IdPaymentType'] = 4;
                        else $arResult['IdPaymentType'] = 4;
                    } else {
                        $arResult['IdPaymentType'] = 5;
                    }
                     $accid_date = Converter::daysToDate($accid['CREATIONDATE']);
                }
                
                if ($m['V012']) {
                    $arResult['IdCaseResult'] = current($this->rDB->query("SELECT CODE_EGISZ FROM OMS_CLASSIFIERV012 WHERE ENABLED=1 AND ID=".$m['V012']))['CODE_EGISZ'];
                }
                if ($m['V009']) {
                    $dopAr['IdAmbResult'] = current($this->rDB->query("SELECT CODE_EGISZ FROM OMS_CLASSIFIERV009 WHERE ENABLED=1 AND ID=".$m['V009']))['CODE_EGISZ'];
                }
                
                $IdDiagnosisType = 1;
                if ($m['DISEASE']) {
                    $disease = current($this->rDB->query("SELECT ID FROM OMS_CLASSIFIERDISEASE WHERE ENABLED=1 AND ID=".$m['DISEASE']));
                    if ($disease) {
                        $IdDiagnosisType = $disease['ID'] == 3 ? 2 : ($disease['ID'] == 4 ? 3 : 1);
                    }
                }
                if ($m['MKB10ID']) {
                    $mkbcode = current($this->rDB->query("SELECT CODE FROM MKB10 WHERE ENABLED=1 AND ID=".$m['MKB10ID']))['CODE'];
                    $mkbcode = preg_replace('~([A-Za-zА-Яа-я][0-9]+\.[0-9]+)([A-Za-zА-Яа-я])~iu', '$1', $mkbcode);
                    //TODO нужно на лету проверять из сервиса терминологий
                }

                if ($mkbcode && $m['MKBINFO'])
                    $medRecs[] = [
                        '@attributes' => [
                            'i:type' => 'ClinicMainDiagnosis',
                            'xmlns' => 'http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag',
                        ],
                        'DiagnosisInfo' => [
                            'DiagnosedDate' => $cTime,
                            'IdDiagnosisType' => $IdDiagnosisType,
                            'Comment' => $m['MKBINFO'],
                            'MkbCode' => $mkbcode,
                        ],
                        'Doctor' => $epmzDocAttr
                    ];
                //TODO нужно выделить массив Complications - осложнения
                
                $mzCode = DEBUG_MODE ? 'A01.01.001.001' : '';

                if ($accid['SERVICE']) {
                    $s = current($this->rDB->query("SELECT MZCODE,NAME FROM SERVICES WHERE ENABLED=1 AND ID=".$accid['SERVICE']));
                    $serviceName = $s['NAME'];
                    if ($s['MZCODE']) $mzCode = $s['MZCODE']; // TODO выдавать ошибка
                }

                if ($mzCode && $accid_date && $serviceName)
                $medRecsSteps[] = [
                    'DateEnd' => $accid_date,
                    'DateStart' => $accid_date,
                    'IdServiceType' => $mzCode,
                    'ServiceName' => $serviceName,
                ];
            }

            $amb = [
                'DateStart' => $cTime,
                'DateEnd' => $cTime,
            ];
            if ($arResult['IdPaymentType']) $amb['IdPaymentType'] = $arResult['IdPaymentType'];
            
            if ($epmzDoc) $amb['Doctor'] = $this->copyAddXmlns($epmzDoc);
            $amb['IdStepMis'] = $e['ID'];
            $amb['IdVisitPlace'] = '1';

            if ($ar['RECEPTIONTYPE']) {
                $amb['IdVisitPurpose'] = current($this->rDB->query("SELECT CODE_EGISZ FROM RECEPTION_TYPES WHERE ENABLED=1 AND ID=".$ar['RECEPTIONTYPE']))['CODE_EGISZ'];
            }
            
            foreach ($medRecsSteps as $m) {
                $this->addXmlns($m['Performer']);
                $m['Performer']['Doctor'] = $epmzDoc;
                $m['Performer']['IdRole'] = '3';
                $s = $m['ServiceName'];
                unset($m['ServiceName']);
                $m['ServiceName'] = $s;
                $m['@attributes'] = ['i:type' => "Service"];
                $amb['MedRecords']['MedRecord'][] = $m;
            }

            if ($e['EPMZTYPE'] == 9){
                $medRecs[] = (new SickList($this->rDB, $e, $epmzDocAttr))->get();
                $medRecs[] = (new ClinicMainDiagnosis($this->rDB, $e, $epmzDocAttr))->get();
            } else if ($e['EPMZTYPE'] == 8){
                $amb['MedRecords']['MedRecord'][] = (new LaboratoryReport($this->rDB, $e, $epmzDocAttr))->get();
            } else {
                $amb['MedRecords']['MedRecord'][] = (new ConsultNote($this->rDB, $e, $epmzDocAttr))->get();
            }

            $this->addXmlns($amb['MedRecords'], 'MedRec');

            $Steps[] = $amb;

            //dump($mkb10);
            
//            if ($e['CREATOR']) {
//                $prof = current($this->rDB->query("SELECT PROF FROM USERS WHERE ENABLED=1 AND ID=".$e['CREATOR']));
//            }
        }
        //dump($epmz);
        
        $medcard = current($this->rDB->query("SELECT ID,BIRTHDATE,GENDERTYPE FROM MEDCARDS WHERE ENABLED=1 AND ID=".$ar['MCID']));
        if ($medcard) {
            $medcard['BIRTHDATE'] = Converter::daysToDate($medcard['BIRTHDATE'],true);
            $medcard['GENDERTYPE'] = intval($medcard['GENDERTYPE'])+1;
            $IdPatientMis = md5($medcard['ID'].$medcard['BIRTHDATE'].$medcard['GENDERTYPE']);
        }


        if ($DoctorInCharge) {
            $arResult['DoctorInCharge'] = $DoctorInCharge;
            $arResult['Authenticator']['Doctor'] = $epmzDoc; // разную выборку нужно делать
            $arResult['Author']['Doctor'] = $DoctorInCharge;

            if ($IdPatientMis) $arResult['IdPatientMis'] = $IdPatientMis;
            $arResult['CaseVisitType'] = $ar['ISFIRSTRECEPTION'];
            $arResult['IdCasePurpose'] = $amb['IdVisitPurpose'];
            $arResult['IdCaseType'] = $dopAr['IdCaseType'];
            if ($dopAr['IdAmbResult']) $arResult['IdAmbResult'] = $dopAr['IdAmbResult'];
        }

        $arResult['Steps']['StepAmb'] = $Steps;

        if (empty($medRecs)){
            // В данном случае не выгрузится тк ClinicMainDiagnosis обязателен
            App::log("AddCase - MedRecords.ClinicMainDiagnosis отсутсвует","Запись с id=".$ar['ID']." не будет выгружена");
            $this->rDB->deleteLog([$ar['ID']]);
            $this->addAttr($arResult['MedRecords'], ['i:nil' => 'true']);
        } else {
            $arResult['MedRecords']['MedRecord'] = $medRecs;
        }

        $this->addXmlnsForAll($arResult, ['DoctorInCharge', 'Authenticator', 'Author']);
        $this->addXmlnsForAll($arResult, ['Steps'], 'Step');
        $this->addXmlnsForAll($arResult, ['MedRecords'], 'MedRec');

        return $arResult;
    }
    
    public function updateCase_changeFields( $ar = []){
        $arResult = $this->addCase_changeFields($ar);
        //$arResult['CloseDate'] = $arResult['OpenDate'];
        return $arResult;
    }
    
    /**
     * Перевод данных из бд mssql в mysql
     * !!! очень важен порядок элементов в массиве
     * @param array $ar - массив из mssql
     * @return array
     */
    public function closeCase_changeFields( $ar = []){
        $arResult = [];
        $dateFileds = [
            'RECEPTIONDATETIME' => 1,
            'DATETIME' => 1,
            'RECEPTIONCLOSEDATETIME' => 1,
        ];
        
        $dopAr = [];

        foreach ($ar as $k=>$v) {
            $t = isset(Converter::$matchCase[$k]) ? Converter::$matchCase[$k]: false;
            if ($t && $v != null) {
                if (isset($dateFileds[$k])) $v = Converter::daysToDate($v,($t == 'OpenDate' /*|| $t == 'CloseDate'*/));
                $dopAr[$t] = (string)$v;
            }
            
        }
        
        $arResult['OpenDate'] = $dopAr['OpenDate'];
        $arResult['CloseDate'] = $dopAr['CloseDate'];
        $arResult['HistoryNumber'] = $dopAr['HistoryNumber'];
        $arResult['IdCaseMis'] = $dopAr['HistoryNumber'];
        $arResult['IdPaymentType'] = '1';
        $arResult['Confidentiality'] = '1';
        $arResult['DoctorConfidentiality'] = '1';
        $arResult['CuratorConfidentiality'] = '1';
        $arResult['IdLpu'] = $this->con['idLPU'];
        $arResult['IdCaseResult'] = '1';
        $arResult['Comment'] = $dopAr['Comment']?:'-';
        
        $medRecs = [];
        
        $epmz = $this->rDB->query("SELECT * FROM EPMZ WHERE ENABLED=1 AND RECEPTIONID=".$ar['ID']);
        //dump($epmz);

        $DoctorInCharge = $this->getDoctors($ar);


        foreach ($epmz as $e) {
            $cTime = Converter::daysToDate($e['CREATIONDATETIME']);
            $mkb10 = $this->rDB->query("SELECT * FROM EPMZ_MKB10 WHERE ENABLED=1 AND EPMZID=".$e['ID']);
            //dump($mkb10);
            $medRecsSteps = [];
            $epmzDoc = $this->getDoctors($e,true);
            $epmzDocAttr = $this->copyAddXmlns($epmzDoc);

            foreach ($mkb10 as $m) {
                $accid = current($this->rDB->query("SELECT * FROM ACCOUNTS_CONTENTS WHERE ENABLED=1 AND ID=".$m['ACCID']));
                //dump($accid);
                if ($accid) {
                    if ($accid['OWNERACCOUNT'] > 0 && $accid['POLISEID'] > 0) {
                        $police = current($this->rDB->query("SELECT TYPEOMS FROM POLISES WHERE ENABLED=1 AND ID=".$accid['POLISEID']));
                        if ($police['TYPEOMS'] == 1) $arResult['IdPaymentType'] = 1;
                        else if ($police['TYPEOMS'] > 1) $arResult['IdPaymentType'] = 4;
                        else $arResult['IdPaymentType'] = 4;
                    } else {
                        $arResult['IdPaymentType'] = 5;
                    }
                }
                
                if ($m['V012']) {
                    $arResult['IdCaseResult'] = current($this->rDB->query("SELECT CODE_EGISZ FROM OMS_CLASSIFIERV012 WHERE ENABLED=1 AND ID=".$m['V012']))['CODE_EGISZ'];
                }
                /*if ($m['V009']) {
                    $dopAr['IdAmbResult'] = current($this->rDB->query("SELECT CODE_EGISZ FROM OMS_CLASSIFIERV009 WHERE ENABLED=1 AND ID=".$m['V009']))['CODE_EGISZ'];
                }*/
                
                $IdDiagnosisType = 1;
                if ($m['DISEASE']) {
                    $disease = current($this->rDB->query("SELECT ID FROM OMS_CLASSIFIERDISEASE WHERE ENABLED=1 AND ID=".$m['DISEASE']));
                    if ($disease) {
                        $IdDiagnosisType = $disease['ID'] == 3 ? 2 : ($disease['ID'] == 4 ? 3 : 1);
                    }
                }
                if ($m['MKB10ID']) {
                    $mkbcode = current($this->rDB->query("SELECT CODE FROM MKB10 WHERE ENABLED=1 AND ID=".$m['MKB10ID']))['CODE'];
                    $mkbcode = preg_replace('~([A-Za-zА-Яа-я][0-9]+\.[0-9]+)([A-Za-zА-Яа-я])~iu', '$1', $mkbcode);
                    //TODO нужно на лету проверять из сервиса терминологий
                }

                if ($mkbcode)
                    $medRecs[] = [
                        '@attributes' => [
                            'i:type' => 'ClinicMainDiagnosis',
                            'xmlns' => 'http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag',
                        ],
                        'DiagnosisInfo' => [
                            'DiagnosedDate' => $cTime,
                            'IdDiagnosisType' => $IdDiagnosisType,
                            'Comment' => $m['MKBINFO'],
                            'MkbCode' => $mkbcode,
                        ],
                        'Doctor' => $epmzDocAttr

                    ];
            }
                        
//            if ($e['CREATOR']) {
//                $prof = current($this->rDB->query("SELECT PROF FROM USERS WHERE ENABLED=1 AND ID=".$e['CREATOR']));
//            }

            if ($e['EPMZTYPE'] == 9){
                $medRecs[] = (new SickList($this->rDB, $e, $epmzDocAttr))->get();
                $medRecs[] = (new ClinicMainDiagnosis($this->rDB, $e, $epmzDocAttr))->get();
            } else if ($e['EPMZTYPE'] == 8){
                $medRecs[] = (new LaboratoryReport($this->rDB, $e, $epmzDocAttr))->get();
            } else {
                $medRecs[] = (new ConsultNote($this->rDB, $e, $epmzDocAttr))->get();
            }
        }
        //dump($epmz);
        
        $medcard = current($this->rDB->query("SELECT ID,BIRTHDATE,GENDERTYPE FROM MEDCARDS WHERE ENABLED=1 AND ID=".$ar['MCID']));
        if ($medcard) {
            $medcard['BIRTHDATE'] = Converter::daysToDate($medcard['BIRTHDATE'],true);
            $medcard['GENDERTYPE'] = intval($medcard['GENDERTYPE'])+1;
            $IdPatientMis = md5($medcard['ID'].$medcard['BIRTHDATE'].$medcard['GENDERTYPE']);
        }

        if ($DoctorInCharge) {
            $arResult['DoctorInCharge'] = $DoctorInCharge;
            $arResult['Authenticator']['Doctor'] = $epmzDoc;
            $arResult['Author']['Doctor'] = $DoctorInCharge;
            
            if ($IdPatientMis) $arResult['IdPatientMis'] = $IdPatientMis;
            $arResult['CaseVisitType'] = $ar['ISFIRSTRECEPTION'];
        }

        if (empty($medRecs)){
            // В данном случае не выгрузится тк ClinicMainDiagnosis обязателен
            App::log("AddCase - MedRecords.ClinicMainDiagnosis отсутсвует","Запись с id=".$ar['ID']." не будет выгружена");
            $this->rDB->deleteLog([$ar['ID']]);
            $this->addAttr($arResult['MedRecords'], ['i:nil' => 'true']);
        } else {
            $arResult['MedRecords']['MedRecord'] = $medRecs;
        }

        $this->addXmlnsForAll($arResult, ['DoctorInCharge', 'Authenticator', 'Author']);
        $this->addXmlnsForAll($arResult, ['MedRecords'], 'MedRec');

        return $arResult;
    }
    /**
     * Перевод данных из бд mssql в mysql
     * !!! очень важен порядок элементов в массиве
     * @param array $ar - массив из mssql
     * @return array
     */
    public function createCase_changeFields( $ar = []){
        $arResult = [];
        $dateFileds = [
            'RECEPTIONDATETIME' => 1,
            'DATETIME' => 1,
            'RECEPTIONCLOSEDATETIME' => 1,
        ];
        
        $dopAr = [];
        
        foreach ($ar as $k=>$v) {
            $t = isset(Converter::$matchCase[$k]) ? Converter::$matchCase[$k]: false;
            if ($t && $v != null) {
                if (isset($dateFileds[$k])) $v = Converter::daysToDate($v,($t == 'OpenDate' /*|| $t == 'CloseDate'*/));
                $dopAr[$t] = (string)$v;
            }
            
        }

        $arResult['OpenDate'] = $dopAr['OpenDate'];
        $arResult['HistoryNumber'] = $dopAr['HistoryNumber'];
        $arResult['IdCaseMis'] = $dopAr['HistoryNumber'];
        $arResult['IdPaymentType'] = '1';
        $arResult['Confidentiality'] = '1';
        $arResult['DoctorConfidentiality'] = '1';
        $arResult['CuratorConfidentiality'] = '1';
        $arResult['IdLpu'] = $this->con['idLPU'];
        
        $Steps = [];
        $medRecs = [];
        
        $epmz = $this->rDB->query("SELECT * FROM EPMZ WHERE ENABLED=1 AND RECEPTIONID=".$ar['ID']);
        //dump($epmz);

        $DoctorInCharge = $this->getDoctors($ar);

        foreach ($epmz as $e) {
            $cTime = Converter::daysToDate($e['CREATIONDATETIME']);
            $mkb10 = $this->rDB->query("SELECT * FROM EPMZ_MKB10 WHERE ENABLED=1 AND EPMZID=".$e['ID']);
            //dump($mkb10);
            $epmzDoc = $this->getDoctors($e,true);
            $epmzDocAttr = $this->copyAddXmlns($epmzDoc);
            foreach ($mkb10 as $m) {
                $accid = current($this->rDB->query("SELECT * FROM ACCOUNTS_CONTENTS WHERE ENABLED=1 AND ID=".$m['ACCID']));
                //dump($accid);
                if ($accid) {
                    if ($accid['OWNERACCOUNT'] > 0 && $accid['POLISEID'] > 0) {
                        $police = current($this->rDB->query("SELECT TYPEOMS FROM POLISES WHERE ENABLED=1 AND ID=".$accid['POLISEID']));
                        if ($police['TYPEOMS'] == 1) $arResult['IdPaymentType'] = 1;
                        else if ($police['TYPEOMS'] > 1) $arResult['IdPaymentType'] = 4;
                        else $arResult['IdPaymentType'] = 4;
                    } else {
                        $arResult['IdPaymentType'] = 5;
                    }
                }

                $IdDiagnosisType = 1;
                if ($m['DISEASE']) {
                    $disease = current($this->rDB->query("SELECT ID FROM OMS_CLASSIFIERDISEASE WHERE ENABLED=1 AND ID=".$m['DISEASE']));
                    if ($disease) {
                        $IdDiagnosisType = $disease['ID'] == 3 ? 2 : ($disease['ID'] == 4 ? 3 : 1);
                    }
                }
                if ($m['MKB10ID']) {
                    $mkbcode = current($this->rDB->query("SELECT CODE FROM MKB10 WHERE ENABLED=1 AND ID=".$m['MKB10ID']))['CODE'];
                    $mkbcode = preg_replace('~([A-Za-zА-Яа-я][0-9]+\.[0-9]+)([A-Za-zА-Яа-я])~iu', '$1', $mkbcode);
                }
                
                if ($mkbcode && $m['MKBINFO'])
                    $medRecs[] = [
                        '@attributes' => [
                            'i:type' => 'ClinicMainDiagnosis',
                            'xmlns' => 'http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag',
                        ],
                        'DiagnosisInfo' => [
                            'DiagnosedDate' => $cTime,
                            'IdDiagnosisType' => $IdDiagnosisType,
                            'Comment' => $m['MKBINFO'],
                            'MkbCode' => $mkbcode,
                        ],
                        'Doctor' => $epmzDocAttr

                    ];
            }

            $amb = [
                'DateStart' => $cTime,
                'DateEnd' => $cTime,
            ];
            if ($arResult['IdPaymentType']) $amb['IdPaymentType'] = $arResult['IdPaymentType'];
            
            if ($epmzDoc) $amb['Doctor'] = $epmzDocAttr;
            $amb['IdStepMis'] = $e['ID'];
            $amb['IdVisitPlace'] = '1';
            
            if ($ar['RECEPTIONTYPE']) {
                $amb['IdVisitPurpose'] = current($this->rDB->query("SELECT CODE_EGISZ FROM RECEPTION_TYPES WHERE ENABLED=1 AND ID=".$ar['RECEPTIONTYPE']))['CODE_EGISZ'];
            }

            if ($e['EPMZTYPE'] == 9){
                $medRecs[] = (new SickList($this->rDB, $e, $epmzDocAttr))->get();
                $medRecs[] = (new ClinicMainDiagnosis($this->rDB, $e, $epmzDocAttr))->get();

//                $amb['MedRecords']['MedRecord'][] = (new SickList($this->rDB, $e, $epmzDocAttr))->get();
            } else if ($e['EPMZTYPE'] == 8){
                $amb['MedRecords']['MedRecord'][] = (new LaboratoryReport($this->rDB, $e, $epmzDocAttr))->get();
            } else {
                $amb['MedRecords']['MedRecord'][] = (new ConsultNote($this->rDB, $e, $epmzDocAttr))->get();
            }

            $this->addXmlns($amb['MedRecords'], 'MedRec');

            $Steps[] = $amb;
            //dump($mkb10);
        }
        //dump($epmz);
        
        $medcard = current($this->rDB->query("SELECT ID,BIRTHDATE,GENDERTYPE FROM MEDCARDS WHERE ENABLED=1 AND ID=".$ar['MCID']));
        if ($medcard) {
            $medcard['BIRTHDATE'] = Converter::daysToDate($medcard['BIRTHDATE'],true);
            $medcard['GENDERTYPE'] = intval($medcard['GENDERTYPE'])+1;
            $IdPatientMis = md5($medcard['ID'].$medcard['BIRTHDATE'].$medcard['GENDERTYPE']);
        }
        
        $DoctorInCharge = $this->getDoctors($ar);

        if ($DoctorInCharge) {
            $arResult['Authenticator']['Doctor'] = $epmzDoc; // разную выборку нужно делать
            $arResult['Author']['Doctor'] = $DoctorInCharge;
            
            if ($IdPatientMis) $arResult['IdPatientMis'] = $IdPatientMis;
            $arResult['CaseVisitType'] = $ar['ISFIRSTRECEPTION'];
            $arResult['IdCasePurpose'] = $amb['IdVisitPurpose'];
            $arResult['IdCaseType'] = $dopAr['IdCaseType'];
        }
        
        $arResult['Steps']['StepAmb'] = $Steps;

        if (empty($medRecs)){
            $this->addAttr($arResult['MedRecords'], ['i:nil' => 'true']);
        } else {
            $arResult['MedRecords']['MedRecord'] = $medRecs;
        }


        $this->addXmlnsForAll($arResult, ['DoctorInCharge', 'Authenticator', 'Author']);
        $this->addXmlnsForAll($arResult, ['Steps'], 'Step');
        $this->addXmlnsForAll($arResult, ['MedRecords'], 'MedRec');

        return $arResult;
    }
    /**
     * Перевод данных из бд mssql в mysql
     * !!! очень важен порядок элементов в массиве
     * @param array $e - массив из mssql
     * @return array
     */
    public function addStepToCase_changeFields( $e = []){
        $arResult = [];
        
        $ar = current($this->rDB->query("SELECT * FROM RECEPTIONS WHERE enabled=1 AND ID=".$e['RECEPTIONID']));

        $arResult['IdCaseMis'] = $ar['ID'];
        $arResult['IdLpu'] = $this->con['idLPU'];

        $cTime = Converter::daysToDate($e['CREATIONDATETIME']);
        $Step = [
            'DateStart' => $cTime,
            'DateEnd' => $cTime,
            'IdPaymentType' => 1,
        ];
        
        $medRecs = [];

        $mkb10 = $this->rDB->query("SELECT * FROM EPMZ_MKB10 WHERE ENABLED=1 AND EPMZID=".$e['ID']);
        foreach ($mkb10 as $m) {
            $accid = current($this->rDB->query("SELECT * FROM ACCOUNTS_CONTENTS WHERE ENABLED=1 AND ID=".$m['ACCID']));
            if ($accid) {
                if ($accid['OWNERACCOUNT'] > 0 && $accid['POLISEID'] > 0) {
                    $police = current($this->rDB->query("SELECT TYPEOMS FROM POLISES WHERE ENABLED=1 AND ID=".$accid['POLISEID']));
                    if ($police['TYPEOMS'] == 1) $Step['IdPaymentType'] = 1;
                    else if ($police['TYPEOMS'] > 1) $Step['IdPaymentType'] = 4;
                    else $Step['IdPaymentType'] = 4;
                } else {
                    $Step['IdPaymentType'] = 5;
                }
                $accid_date = Converter::daysToDate($accid['CREATIONDATE']);
            }
            
            $mzCode = DEBUG_MODE ? 'A01.01.001.001' : ''; //может и не быть
            
            if ($accid['SERVICE']) {
                $s = current($this->rDB->query("SELECT MZCODE,NAME FROM SERVICES WHERE ENABLED=1 AND ID=".$accid['SERVICE']));
                $serviceName = $s['NAME'];
                if ($s['MZCODE']) $mzCode = $s['MZCODE'];
            }

            if ($mzCode && $accid_date && $serviceName)
                $medRecs[] = [
                    'DateEnd' => $accid_date,
                    'DateStart' => $accid_date,
                    'IdServiceType' => $mzCode,
                    'ServiceName' => $serviceName,
                ];
        }
        
        $medcard = current($this->rDB->query("SELECT ID,BIRTHDATE,GENDERTYPE FROM MEDCARDS WHERE ENABLED=1 AND ID=".$ar['MCID']));
        if ($medcard) {
            $medcard['BIRTHDATE'] = Converter::daysToDate($medcard['BIRTHDATE'],true);
            $medcard['GENDERTYPE'] = intval($medcard['GENDERTYPE'])+1;
            $IdPatientMis = md5($medcard['ID'].$medcard['BIRTHDATE'].$medcard['GENDERTYPE']);
        }
        
        if ($IdPatientMis) $arResult['IdPatientMis'] = $IdPatientMis;//в этом методо этот порядок не важен
        
        $epmzDoc = $this->getDoctors($e,true);
        $epmzDocAttr =   $this->copyAddXmlns($epmzDoc);
        $Step['Doctor'] = $epmzDocAttr;
        $Step['IdStepMis'] = $e['ID'];
        $Step['IdVisitPlace'] = '1';
        
        if ($ar['RECEPTIONTYPE']) {
            $Step['IdVisitPurpose'] = current($this->rDB->query("SELECT CODE_EGISZ FROM RECEPTION_TYPES WHERE ENABLED=1 AND ID=".$ar['RECEPTIONTYPE']))['CODE_EGISZ'];
        }
        
        foreach ($medRecs as $m) {
            $this->addXmlns($m['Performer']);
            $m['Performer']['Doctor'] = $epmzDoc;
            $m['Performer']['IdRole'] = '3';
            $s = $m['ServiceName'];
            unset($m['ServiceName']);
            $m['ServiceName'] = $s;
            $m['@attributes'] = ['i:type' => "Service"];
            $Step['MedRecords']['MedRecord'][] = $m;
        }

        /**
         * В AddStepToCase нельзя отправлять SickList и LaboratoryReport
         * https://api.n3health.ru/docs.php?article=IEMKService#AddStepToCaseMedRecord
         */
        if ($e['EPMZTYPE'] == 9){
//            $Step['MedRecords']['MedRecord'][] = (new SickList($this->rDB, $e, $epmzDocAttr))->get();
        } else if ($e['EPMZTYPE'] == 8){
//            $Step['MedRecords']['MedRecord'][] = (new LaboratoryReport($this->rDB, $e, $epmzDocAttr))->get();
        } else {
            $Step['MedRecords']['MedRecord'][] = (new ConsultNote($this->rDB, $e, $epmzDocAttr))->get();
        }

        $this->addXmlns($Step['MedRecords'], 'MedRec');
        $arResult['Step'] = $Step;

        foreach ($medRecs as $m) {
            $m['Doctor'] = $epmzDoc;
            $arResult['MedRecords']['MedRecord'][] = $m;
        }

//        $arResult['MedRecords']['MedRecord'] = $medRecs;

//        $this->addXmlnsForAll($arResult, ['Doctor']);
//        $this->addXmlnsForAll($arResult, ['MedRecords'], 'MedRec');

        return $arResult;
    }
    
    private function getDoctors($base = [],$epmz = false){
        $result = [];
        if ($epmz) {
            $doctor = current($this->rDB->query("
                SELECT 
                    d.ID as IdPersonMis,
                    d.NAME as FamilyName, 
                    d.NAME1 as GivenName, 
                    d.NAME2 as MiddleName,
                    dt.ID as type_id,
                    dt.CODE_EGISZ as IdSpeciality,
                    p.CODE_EGISZ as IdPosition
                FROM 
                    USERS u
                    LEFT JOIN DOCTORS d on d.USERID = u.ID
                    LEFT JOIN DOCTORTYPES dt on dt.ID = d.TYPE
                    LEFT JOIN PROFS p on p.ID = u.PROFID
                WHERE 
                    u.ENABLED=1 AND 
                    d.ENABLED=1 AND 
                    dt.ENABLED=1 AND 
                    p.ENABLED=1 AND
                    u.ID=".$base['AUTHORUSER']
            ));
        } else {
            $doctor = current($this->rDB->query("
                SELECT 
                    d.ID as IdPersonMis,
                    d.NAME as FamilyName, 
                    d.NAME1 as GivenName, 
                    d.NAME2 as MiddleName,
                    dt.ID as type_id,
                    dt.CODE_EGISZ as IdSpeciality,
                    p.CODE_EGISZ as IdPosition
                FROM 
                    DOCTORS d
                    LEFT JOIN DOCTORTYPES dt on dt.ID = d.TYPE
                    LEFT JOIN USERS u on d.USERID = u.ID
                    LEFT JOIN PROFS p on p.ID = u.PROFID
                WHERE 
                    d.ENABLED=1 AND 
                    dt.ENABLED=1 AND 
                    u.ENABLED=1 AND
                    p.ENABLED=1 AND
                    d.ID=".$base['DOCID']
            ));
        }
        
        if ($doctor) {
            $HumanName = [
                'GivenName' => $doctor['GivenName'],
                'FamilyName' => $doctor['FamilyName'],
            ];
            if ($doctor['MiddleName']) $HumanName['MiddleName'] = $doctor['MiddleName'];
            /*$document = [
                'IdDocumentType' => '1',
                'DocS' => '123456',
                'DocN' => '1111',
                'ProviderName' => 'Уфмс',
                'ExpiredDate' => '2010-12-01T00:00:00',
                'IssuedDate' => '2006-09-03T00:00:00',
                'RegionCode' => '128',
            ];*/
            $person = [
                'HumanName' => $HumanName,
                'IdPersonMis' => $doctor['IdPersonMis'],
            ];
            $result['Person'] = $person;
            $result['IdLpu'] = $this->con['idLPU'];
            if (DEBUG_MODE) {
                $result['IdSpeciality'] = $doctor['IdSpeciality']? : $doctor['type_id'];
                $result['IdPosition'] = '74';
            } else {
                $result['IdSpeciality'] = $doctor['IdSpeciality'];//генерить ошибку если нет CODE_EGISZ
                $result['IdPosition'] = $doctor['IdPosition'];
            }
        }
        return $result;
    }
    
    private function setRecursiveFields($ar,$opt = []){
        $options = array_merge([
            'key' => '',
            'tag' => 'a',
            'depth' => 0,
            'steps' => 0,
            'medrecord' => 0,
            'method' => ''
        ],$opt);

        if ($options['key'] === '@attributes') return '';

        $res = '';
        $scheme = '';
        $options['depth']++;
        
        switch ($options['key']) {
            case 'DoctorInCharge':
            case 'Authenticator':
            case 'Author':
            case 'LegalAuthenticator':
            case 'Guardian':
                $scheme = ' xmlns:b="http://schemas.datacontract.org/2004/07/N3.EMK.Dto"';
            break;
            case 'Steps':
                $scheme = ' xmlns:b="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step"';
                $options['steps'] = 1;
            break;
            case 'MedRecords':
                
                if ($options['method'] == 'AddCase') {
                    if ($options['steps']) {
                        $scheme = ' xmlns:c="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec"';
                    } else {
                        $scheme = ' xmlns:b="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec"';
                    }
                } else {
                    $scheme = ' xmlns:b="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec"';
                    $options['medrecord'] = 1;
                }
            break;
            case 'MedRecord':
                if ($options['method'] == 'AddStepToCase') {
                    $scheme = ' i:type="b:Service"';
                    $options['service'] = 1;
                } elseif ($options['method'] == 'AddCase') {
                    $options['medrecord'] = 1;
                    if ($options['steps']) {
                        $scheme = ' i:type="c:Service"';
                    } else {
                        $options['allMedrec'] = 1;
                        $scheme = ' i:type="c:ClinicMainDiagnosis" xmlns:c="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag"';
                    }
                } else {
                    $scheme = ' i:type="c:Diagnosis" xmlns:c="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag"';
                }
            break;
            case 'Performer':
                if ($options['method'] == 'AddCase') {
                    $scheme = ' xmlns:d="http://schemas.datacontract.org/2004/07/N3.EMK.Dto"';
                    $options['medrecord'] = 2;
                    $options['steps'] = 0;
                } else {
                    $scheme = ' xmlns:c="http://schemas.datacontract.org/2004/07/N3.EMK.Dto"';
                }
            break;
            case 'Doctor':
                if ($options['steps']) {
                    if ($options['method'] == 'AddStepToCase') {
                        $scheme = ' xmlns:b="http://schemas.datacontract.org/2004/07/N3.EMK.Dto"';
                    } else {
                        $scheme = ' xmlns:c="http://schemas.datacontract.org/2004/07/N3.EMK.Dto"';
                    }
                }
                if ($options['medrecord']) {
                    if ($options['method'] == 'AddStepToCase') {
                        $scheme = '';
                    } elseif ($options['method'] == 'AddCase') {
                        if (!$options['allMedrec']) {
                            $scheme = '';
                        } else {
                            $scheme = ' xmlns:d="http://schemas.datacontract.org/2004/07/N3.EMK.Dto"';
                        }
                        $options['medrecord'] = 2;
                    } else {
                        $options['medrecord'] = 2;
                        $scheme = ' xmlns:d="http://schemas.datacontract.org/2004/07/N3.EMK.Dto"';
                    }
                }
            break;
            default :
                $scheme = '';
        }

        if ($options['steps'] && $options['depth']>5) $options['tag'] = 'c';
        elseif ($options['medrecord'] && $options['depth']>3 && !$options['service']) {
            $options['tag'] = ($options['depth'] > 5 && $options['medrecord'] > 1)
                ? 'd' : 'c';
        }
        else if ($options['depth']>2) $options['tag'] = 'b';
        
        $lastTag = true;
        
        if ($options['key'] && is_string($options['key'])) $res .= "<".$options['tag'].":".$options['key'].$scheme.'>';
        if (is_array($ar)) {
            $res .= "\n";
            foreach ($ar as $k=>$v) {
                $res .= $this->setRecursiveFields($v, array_merge($options,['key'=>$k]));
                if (!is_string($k)) {
                    $res .= "</".$options['tag'].":".$options['key'].">"."\n";
                    $lastTag = false;
                    if ($k+1 < count($ar)) $res .= "<".$options['tag'].":".$options['key'].$scheme.'>'."\n";
                }
            }
        } else {
           $res .= $ar;
        }
        if ($options['key'] && is_string($options['key']) && $lastTag) $res .= "</".$options['tag'].":".$options['key'].">"."\n";
        return $res;
    }
    
    /**
     * 
     * @param type $d
     * @return string
     */
    public function addCase($d = []) {
        if (!$_REQUEST['old']) return $this->getXml($d, 'AddCase');
        $request = '
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                <s:Body>
                    <AddCase xmlns="http://tempuri.org/">
                        <guid>'.$this->con['guid'].'</guid>
                        <caseDto i:type="a:CaseAmb" xmlns:a="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">'."\n";
                        $request .= $this->setRecursiveFields($d,[
                            'method' => 'AddCase',
                        ]);
                        $request .= '
                          </caseDto>
                    </AddCase>
                </s:Body>
            </s:Envelope>';
                        dd($request);
        return $request;
    }
    
    public function updateCase($d = []){
        return $this->getXml($d, 'UpdateCase');
//        $request = '
//            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
//                <s:Body>
//                    <UpdateCase xmlns="http://tempuri.org/">
//                        <guid>'.$this->con['guid'].'</guid>
//                        <caseDto i:type="a:CaseAmb" xmlns:a="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">'."\n";
//                        $request .= $this->setRecursiveFields($d,[
//                            'method' => 'AddCase',
//                        ]);
//                        $request .= '
//                          </caseDto>
//                    </UpdateCase>
//                </s:Body>
//            </s:Envelope>';
//        return $request;
    }
    
    public function closeCase($d = []){
        if (!$_REQUEST['old']) return $this->getXml($d, 'CloseCase');
        $request = '
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                <s:Body>
                    <CloseCase xmlns="http://tempuri.org/">
                        <guid>'.$this->con['guid'].'</guid>
                        <caseDto i:type="a:CaseAmb" xmlns:a="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">'."\n";
                        $request .= $this->setRecursiveFields($d,[
                            'method' => 'AddCase',
                        ]);
                        $request .= '
                          </caseDto>
                    </CloseCase>
                </s:Body>
            </s:Envelope>';
        return $request;
    }
    public function createCase($d = []){
        if (!$_REQUEST['old']) return $this->getXml($d, 'CreateCase', 'createCaseDto');
        $request = '
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                <s:Body>
                    <CreateCase xmlns="http://tempuri.org/">
                        <guid>'.$this->con['guid'].'</guid>
                        <createCaseDto i:type="a:CaseAmb" xmlns:a="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">'."\n";
                        $request .= $this->setRecursiveFields($d);
                        $request .= '
                          </createCaseDto>
                    </CreateCase>
                </s:Body>
            </s:Envelope>';
        return $request;
    }
    
    public function addStepToCase($d = []){
        $root = [
            '@attributes' => [
                'xmlns:s' => 'http://schemas.xmlsoap.org/soap/envelope/'
            ],
            's:Body' => [
                'AddStepToCase' => [
                    '@attributes' => [
                        'xmlns' => 'http://tempuri.org/'
                    ],
                    'guid' => $this->con['guid'],
                    'idLpu' => $d['IdLpu'],
                    'idPatientMis' => $d['IdPatientMis'],
                    'idCaseMis' => $d['IdCaseMis'],
                    'step' => array_merge(
                        [
                            '@attributes' => [
                                "i:type" => "a:StepAmb",
                                "xmlns:a" => "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step",
                                "xmlns:i" => "http://www.w3.org/2001/XMLSchema-instance"
                            ]
                        ], Base::addTag($d['Step'])
                    )
                ]
            ]
        ];
        if (!$_REQUEST['old']) return Array2XML::createXMLAndSave('s:Envelope', $root);

        $request = '
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
                <s:Body>
                <AddStepToCase xmlns="http://tempuri.org/">
                    <guid>'.$this->con['guid'].'</guid>
                    <idLpu>'.$d['IdLpu'].'</idLpu>
                    <idPatientMis>'.$d['IdPatientMis'].'</idPatientMis>
                    <idCaseMis>'.$d['IdCaseMis'].'</idCaseMis>
                    <step i:type="a:StepAmb" xmlns:a="http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">'."\n";

                    $request .= $this->setRecursiveFields($d['Step'],[
                        'method' => 'AddStepToCase',
                        'steps' => 1,
                    ]);

                    $request .= '
                    </step>
                </AddStepToCase>
                </s:Body>
            </s:Envelope>';
        return $request;
    }

    function getXml($d, $caseName, $internalCaseName = 'caseDto'){
        $root = [
            '@attributes' => [
                'xmlns:s' => 'http://schemas.xmlsoap.org/soap/envelope/'
            ],
            's:Body' => [
                $caseName => [
                    '@attributes' => [
                        'xmlns' => 'http://tempuri.org/'
                    ],
                    'guid' => $this->con['guid'],
                    $internalCaseName => array_merge(
                        [
                            '@attributes' => [
                                "i:type" => "a:CaseAmb",
                                "xmlns:a" => "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case",
                                "xmlns:i" => "http://www.w3.org/2001/XMLSchema-instance"
                            ]
                        ], Base::addTag($d)
                    )
                ]
            ]
        ];
        return Array2XML::createXMLAndSave('s:Envelope', $root);
    }

    function addXmlns(&$arr = [], $type = null) {
        $type = $type ? '.' . $type : '';
        $arr['@attributes'] = [
            'xmlns' => 'http://schemas.datacontract.org/2004/07/N3.EMK.Dto' . $type
        ];
    }

    function addAttr(&$arr, $attr) {
        $arr['@attributes'] = array_merge($arr['@attributes'] ?? [], $attr);
    }
    function copyAddXmlns($arr = [], $type = null) {
        $type = $type ? '.' . $type : '';
        $arr['@attributes'] = [
            'xmlns' => 'http://schemas.datacontract.org/2004/07/N3.EMK.Dto' . $type
        ];
        return $arr;
    }

    function addXmlnsForAll(&$arr, $fields, $type = null) {
        foreach ($fields as $field) {
            if (isset($arr[$field])){
                $this->addXmlns($arr[$field], $type);
            }
        }
    }
}