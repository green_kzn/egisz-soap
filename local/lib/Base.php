<?php


abstract class Base {

    static function addTag($ar, $tag = 'a'){
        $res = [];
        foreach ($ar as $key => $item) {
            if ($key === 'xmlns') {
                $key = "xmlns:$tag";
            } else if ($key === 'i:type') {
                $item = "$tag:$item";
            } else if (is_string($key) && $key != '@attributes' && !preg_match("~:~", $key)) {
                $key = "$tag:$key";
            }

            if (is_array($item)) {
                $t = isset($item['@attributes']['xmlns']) ? chr(ord($tag) + 1) : $tag;
                $item = self::addTag($item, $t);
            }


            $res[$key] = $item;
        }

        return $res;
        return $first ? $res : [$res, $changeTag];
    }

}