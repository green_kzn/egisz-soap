<?php
class DB {
    
    public $db;
    public $driver = 'mysql';
        
    public function __construct( $settings = [] ) {
        $settings = (object)$settings;
        if (!isset($settings->port)) $settings->port = 22;
        $this->settings = $settings;
        try {
            if ($this->driver == 'mysql') {
                $this->db = new PDO("mysql:host=$settings->host;dbname=$settings->db;", $settings->user, $settings->pass);
            } else {
                $this->db = new PDO("sqlsrv:Server=$settings->host,$settings->port;Database=$settings->db", $settings->user, $settings->pass);
            }
        } catch (PDOException $e) {
            App::log("БД",'Подключение не удалось: ' . $e->getMessage()." бд - $this->driver");
        }
    }
    
    public function query($s = '',$exec = false,$arResult = []){
        if ($s) {
            if ($exec) {
                /*try {
                    $arResult = $this->db->exec($s);
                } catch (PDOException $e) {
                    dump($e->getCode().' '.$e->getMessage());
                }*/
                $arResult = $this->db->exec($s);
            } else {
                $sql = $this->db->query($s);
                if (is_object($sql)) $arResult = $sql->fetchAll(PDO::FETCH_ASSOC);
            }
        }
        return $arResult;
    }
    
    public function insert($table='', $ar = []){
        if ($table && $ar) {
            foreach ($ar as $k=>$v) {
                $ar[$k] = "'".$v."'";
            }
            $this->db->exec("INSERT INTO $table (".implode(',',array_keys($ar)).") VALUES (".implode(',',$ar).")");
        }
    }
}