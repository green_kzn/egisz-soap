<?php
class Netrika {
    
    //const GUID = 'bd95b30a-dd4d-40a1-9a39-054acec16c78';
    
    private $con;
    public $type = '';
    public $url = '';
    public $prefixMethod = '';
    
    private $arMess = [
        'propertyname','message','errorcode'
    ];
    
    /**
     * Основной метод создающий подключение к soap сервису
     * @param string $type - префик нужного soap файла
     */
    public function __construct($type = 'Pix') {
        try {
            $this->type = $type;
            $this->url = "http://r78-demo-b2b.n3health.ru/emk/".$type."Service.svc";
            $this->prefixMethod = "http://tempuri.org/I".$type."Service/";
            
            $this->con = new SoapClient($this->url."?wsdl",[
                'soap_version'=>SOAP_1_1,
                'cache_wsdl' => WSDL_CACHE_NONE, // WSDL_CACHE_MEMORY
                'trace' => 1,
                'exception' => 1,
                'keep_alive' => false,
                'encoding' => 'UTF-8',
                'connection_timeout' => 30,
            ]);
        } catch (SoapFault $e) {
            App::log("Soap","Подключение не удалось ".$e->faultcode.','.$e->faultstring);
        }
    }
    
    /**
     * Метод добавляет, обновляет карточку пациента
     * @param array $arParameters - входящие данные для обновления
     * @return boolean
     */
    public function addCard( $arParameters = [] ){
        $arResult = [];
        $c = 0;
        try {
            foreach ($arParameters as $k=>$p) {
                $patient = $this->con->__doRequest($p['xml']['GetPatient'], $this->url, $this->prefixMethod.'GetPatient', SOAP_1_1);
                preg_match('~<getpatientresult.*?>(.*)</getpatientresult>~iu', $patient,$match);
                if ($match[1]) {
                    $method = 'UpdatePatient';
                    $res = $this->con->__doRequest($p['xml']['UpdatePatient'], $this->url, $this->prefixMethod.'UpdatePatient', SOAP_1_1);
                } else {
                    $method = 'AddPatient';
                    $res = $this->con->__doRequest($p['xml']['AddPatient'], $this->url, $this->prefixMethod.'AddPatient', SOAP_1_1);
                }
                if ($er = $this->getError($res)) {
                    App::log("Netrika","Ошибка ".$er);
                    App::log("Неккоректные данные","Запись с id=".$k." не будет выгружена. IdPatientMIS=".$p['info']['IdPatientMIS']);
                    $link = App::logPhp($p['info']);
                    App::log("Файл-лог из бд",$link);
                    $link = App::logXml($p['xml'][$method],'request');
                    App::log("Файл-лог xml запрос",$link);
                    $link = App::logXml($res,'response');
                    App::log("Файл-лог xml ответ",$link);
                } else {
                    $arResult[] = $k;
                    $c++;
                    App::log("Netrika","Обновлена запись IdPatientMIS=".$p['info']['IdPatientMIS']);
                    $link = App::logPhp($p['info']);
                    App::log("Файл-лог из бд",$link);
                    $link = App::logXml($p['xml'][$method],'request');
                    App::log("Файл-лог xml запрос",$link);
                    $link = App::logXml($res,'response');
                    App::log("Файл-лог xml ответ",$link);
                }

            }
            App::log("Netrika","Записей изменено ".$c);
            return $arResult;
        } catch (SoapFault $e) {
            App::log("Netrika","Ошибка $e->faultcode ($e->faultstring)");
            dump($this->con);
        }
        
        return false;
    }
        
    /**
     * Методы ля для Case ф-ий 
     * возвращает tid записей из лога mssql
     * CREATE_CASE / ADD_STEP_TO_CASE / ADD_CASE / CLOSE_CASE
     * @param array $arParameters - входящие данные для обновления
     * @param string $method - текущий метод
     * @return array
     */
    public function caseFunctions($arParameters = [],$method = ''){
        $arResult = [];
        $c = 0;
        try {
            foreach ($arParameters as $k=>$p) {
                $res = $this->con->__doRequest($p['xml'], $this->url, $this->prefixMethod.$method, SOAP_1_1);
                if ($er = $this->getError($res)) {
                    App::log("Netrika-Сбор статистики","Ошибка ".$er);
                    App::log("Неккоректные данные","Запись с id=".$k." не будет выгружена. IdPatientMis=".$p['info']['IdPatientMis']);
                    $link = App::logPhp($p['info']);
                    App::log("Файл-лог из бд",$link);
                    $link = App::logXml($p['xml'],'request');
                    App::log("Файл-лог xml запрос",$link);
                    $link = App::logXml($res,'response');
                    App::log("Файл-лог xml ответ",$link);
                } else {
                    $arResult[] = $k;
                    $c++;
                    App::log("Netrika-Сбор статистики","Обновлена запись IdPatientMis=".$p['info']['IdPatientMis']);
                    $link = App::logPhp($p['info']);
                    App::log("Файл-лог из бд",$link);
                    $link = App::logXml($p['xml'],'request');
                    App::log("Файл-лог xml запрос",$link);
                    $link = App::logXml($res,'response');
                    App::log("Файл-лог xml ответ",$link);
                }

            }
            if ($arResult) App::log("Netrika-Сбор статистики","Записей изменено ".$c." ($method)");
            return $arResult;
        } catch (SoapFault $e) {
            App::log("Netrika-Сбор статистики","Ошибка $e->faultcode ($e->faultstring)");
            dump($this->con);
        }
    }
    
    /**
     * Выделение текста ошибки из xml
     * @param xml $res - xml данные
     * @return boolean|string
     */
    private function getError($res = ''){
        preg_match('~<faultstring.*?>(.*)</faultstring>~iu', $res,$match);
        if ($match[1]) {
            $errors = [];
            foreach ($this->arMess as $i=>$m) {
                preg_match_all("~<$m>(.*?)</$m>~iu", $res,$error);
                if ($error[1]) foreach ($error[1] as $j=>$e) {
                    $errors[$j][$m] = $e;
                }
            }
            foreach ($errors as $i=>$e) {
                $match[1] .= " * ".($i+1).".".implode(':',$e).";";
            }
            return $match[1];
        }
        return false;
    }
}