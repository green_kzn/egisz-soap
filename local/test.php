<?php

$module = 'soap';
$functions = get_extension_funcs($module);
echo "Functions available in the test extension:<br>\n";
foreach($functions as $func) {
    echo $func."\n";
}

die();


require_once($_SERVER['DOCUMENT_ROOT'].'/local/prolog.php');

$json = '{"OpenDate":"2020-04-27T00:00:00","CloseDate":"2020-04-27T10:37:49","HistoryNumber":"237","IdCaseMis":"237","IdPaymentType":5,"Confidentiality":"1","DoctorConfidentiality":"1","CuratorConfidentiality":"1","IdLpu":"bb83867d-7de0-4c27-a473-fa5d5dbadca4","IdCaseResult":"3","Comment":"-","DoctorInCharge":{"Person":{"HumanName":{"GivenName":"ArchiMed+","FamilyName":"\u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440"},"IdPersonMis":"6"},"IdLpu":"bb83867d-7de0-4c27-a473-fa5d5dbadca4","IdSpeciality":"161","IdPosition":"111"},"Authenticator":{"Doctor":{"Person":{"HumanName":{"GivenName":"ArchiMed+","FamilyName":"\u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440"},"IdPersonMis":"6"},"IdLpu":"bb83867d-7de0-4c27-a473-fa5d5dbadca4","IdSpeciality":"161","IdPosition":"111"}},"Author":{"Doctor":{"Person":{"HumanName":{"GivenName":"ArchiMed+","FamilyName":"\u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440"},"IdPersonMis":"6"},"IdLpu":"bb83867d-7de0-4c27-a473-fa5d5dbadca4","IdSpeciality":"161","IdPosition":"111"}},"IdPatientMis":"52832c5ab5286a5b55da81e1b2e191b5","CaseVisitType":"1","IdCasePurpose":"1","IdCaseType":"2","IdAmbResult":"8","Steps":{"StepAmb":[{"DateStart":"2020-04-27T10:34:36","DateEnd":"2020-04-27T10:34:36","IdPaymentType":5,"Doctor":{"Person":{"HumanName":{"GivenName":"ArchiMed+","FamilyName":"\u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440"},"IdPersonMis":"6"},"IdLpu":"bb83867d-7de0-4c27-a473-fa5d5dbadca4","IdSpeciality":"161","IdPosition":"111"},"IdStepMis":"266","IdVisitPlace":"1","IdVisitPurpose":"1","MedRecords":{"MedRecord":[{"DateEnd":"2020-04-27T10:06:10","DateStart":"2020-04-27T10:06:10","IdServiceType":"B01.047.001","Performer":{"Doctor":{"Person":{"HumanName":{"GivenName":"ArchiMed+","FamilyName":"\u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440"},"IdPersonMis":"6"},"IdLpu":"bb83867d-7de0-4c27-a473-fa5d5dbadca4","IdSpeciality":"161","IdPosition":"111"},"IdRole":"3"},"ServiceName":"\u041f\u0435\u0440\u0432\u0438\u0447\u043d\u044b\u0439 \u043f\u0440\u0438\u0435\u043c \u0442\u0435\u0440\u0430\u043f\u0435\u0432\u0442\u0430"},{"type":"SickList","scheme":"http:\/\/schemas.datacontract.org\/2004\/07\/N3.EMK.Dto.MedRec.MedDoc","SickListInfo":{"Number":"","DateStart":"","DateEnd":"","DisabilityDocReason":"","DisabilityDocState":1}}]}}]},"MedRecords":{"MedRecord":[{"DiagnosisInfo":{"DiagnosedDate":"2020-04-27T10:34:36","IdDiagnosisType":3,"Comment":"g","MkbCode":"C01"},"Doctor":{"Person":{"HumanName":{"GivenName":"ArchiMed+","FamilyName":"\u0410\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u043e\u0440"},"IdPersonMis":"6"},"IdLpu":"bb83867d-7de0-4c27-a473-fa5d5dbadca4","IdSpeciality":"161","IdPosition":"111"}}]}}';


$ar = json_decode($json, true);

ini_set('display_errors' ,1);

$ar = ['caseDto' => [
    'hello' => 'frfr',
    'frello' => 'value',
    'array' => [
        'hus' => 1,
        'bus' => 'eerer'
    ],
    'MedRecords' => [
        '@attributes' => [
            'xmlns' => 'dtototoot',
        ],
        'MedRecord' => [
            [
                '@attributes' => [
                    'i:type' => 'SickList',
                ],
                'Info' => [
                    'hello' => 'value',
                    'fuck' => 'bitch'
                ]
            ],
            [
                'Info2' => [
                    'hello2' => 'value2',
                    'fuck2' => 'bitch2'
                ]
            ],
        ]
    ],
    'Steps' => [
        'Date' => 12
    ]
]];

$ar = Base::addTag($ar);

//dd($ar);

dd(Array2XML::createXMLAndSave('root', $ar));

dd(setRecursive(0, $ar));


function setRecursive($key, $val, $options = ['depth' => -1]){
//    print_r($val);
    $options['depth']++;
    $tab = "\n".str_repeat(' ',$options['depth'] * 4 );
    if ($options['depth'] > 4){
        return '';
    }
    if (!is_array($val)){
//        dd('ok');
        return "$tab<$key>$val</$key>";
    } else {
        $res = '';
        if (is_string($key)) $res .= "$tab<$key>";
//            else $options['key'] = 0
        foreach ($val as $k => $item) {
//            print_r($val);
            if (!is_string($key)) $res .= "$tab<{$item['tag']}>";

            $res .= setRecursive($k, $item, $options);

            if (!is_string($key)) $res .= "$tab<{$item['tag']}>";
        }
        if (is_string($key)) $res .= "$tab</$key>";
        return $res;
    }

}